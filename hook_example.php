<?php

function hook($data) {
    #начинаем просматривать каждый заказ по очередь
    for($i=0; $i < count($data); $i++) {

        #берем адрес.
        #обычно, он представляется в формате: г. Санкт-Петербург, Дачный пр., д. 9 {11:00 19:00} вещевая ярмарка 2-й этаж
        $current_address = $data[$i]['location']['address'];
        $reg = '/\{(.*?)\}/';

        #вырезаем данные в скобках
        $res = preg_match($reg, $current_address, $matches);

        #если скобки есть, то нам необходимо преобразовывать данные
        if($res) {
            #режем стоку с временем по пробелу
            $time = explode(' ', $matches[1]);
            $time_window = array('start_time'=>$time[0], 'end_time'=>$time[1]);
            #преобразовываем данные так как нам нужно
            $current_address = explode($matches[0], $current_address);
            $current_address = $current_address[0];
            $additionalInfo = $current_address[1];

            $data[$i]['location']['address'] = $current_address;
            $data[$i]['location']['name'] = $current_address;
            $data[$i]['dropWindows']['dropWindow']['start'] = join(' ', array($data[$i]['date'], $time_window['start_time']));
            $data[$i]['dropWindows']['dropWindow']['end'] = join(' ', array($data[$i]['date'], $time_window['end_time']));

            if(isset($data[$i]['additionalInstructions'])) {
                $data[$i]['additionalInstructions'] = join(', ', array($data[$i]['additionalInstructions'],$additionalInfo));
            } else {
                $data[$i]['additionalInstructions'] = $additionalInfo;
            }

        }

    }

    return $data;
}